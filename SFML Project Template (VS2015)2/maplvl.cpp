#include "maplvl.h"
using namespace sf;


maplvl::maplvl(sf::String path, float widthB, float heightB)
{
	setSizeBlock(widthB,heightB);
	setTitleForMap(path);
}

void maplvl::setSizeBlock(float width, float height)
{
	_widthBlock = width;
	_heightBlock = height;
}

void maplvl::setSizeMap(float width, float height)
{
	_width = width;
	_height = height;
}

void maplvl::setMap(sf::String* sMap)
{
	_map = sMap;
}




void maplvl::setTitleForMap(sf::String path)
{
	mapImage.loadFromFile("image/" + path);
	mapTexture.loadFromImage(mapImage);
	mapSprite.setTexture(mapTexture);
}

void maplvl::addBlock( block posBl)
{
	_blocks.push_back(posBl);
}

void maplvl::drawMap(RenderWindow& window)
{
	for (int i = 0; i < _height; i++)
	{
		for (int j = 0; j < _width; j++)
		{
			for (size_t k = 0; k < _blocks.size(); k++)
			{
				if (_map[i][j] == _blocks[k].st)
				{
					mapSprite.setTextureRect(IntRect(_blocks[k].x,_blocks[k].y, _widthBlock, _heightBlock));

				}
			}

			mapSprite.setPosition(j * _widthBlock, i * _heightBlock);//�� ���� ����������� ����������, ��������� � �����. �� ���� ������ ������� �� ��� �������. ���� ������, �� ��� ����� ���������� � ����� �������� 32*32 � �� ������ ���� �������

			window.draw(mapSprite);//������ ���������� �� �����
		}
	}

}

maplvl::~maplvl()
{
}
