#pragma once

#include "entity.h"



class character:public entity
{
private:
	std::vector<block> _blocks;
	bool onGround = false;//checking on ground
	sf::String* _TileMap;
public:
	character(sf::String image, float posImageCharacterX, float posImageCharacterY, float widthImageCharacter, float heightImageCharacter, float posx, float posy)
		:entity(image, posImageCharacterX, posImageCharacterY, widthImageCharacter, heightImageCharacter, posx, posy) {}
	
	
	void setDefault();

	void setRotation(rotation dir);
	void addBlock(std::vector <block> blocks);
	void setObject(objectLvl * object);
	void setMap(sf::String* map);
	void setScale(float a, float b)
	{
		_Sprite.setScale(a, b);
	}
	virtual void moveCharacter() = 0;

	void animation(float posImageCharacterX, float posImageCharacterY, float widthImageCharacter, float heightImageCharacter);
	void update();
	
	void checkCollisionWithMap(float Dx, float Dy);
	int getHealth();
	
};

