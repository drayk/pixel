#pragma once
#include<SFML/Graphics.hpp>

class viewCamera
{
private:
	sf::View view;
	float _X = 0, _Y = 0;
public:
	viewCamera(float firstX, float firstY, float secondX, float secondY);
	void setCameraPosition(float X, float Y);
	~viewCamera();
	sf::View& getView()
	{
		return view;
	}
};

