#include"maplvl.h"
#include "player.h"
#include"map.h"
#include"viewCamera.h"
using namespace sf;

block setBlock(sf::String st, int x, int y)
{
	block bl;
	bl.x = x;
	bl.y = y;
	bl.st = st;

	return bl;
}


int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
	player hero("rogue.png", 1, 16, 10, 14, 50, 50);
	hero.setScale(2, 2);



	viewCamera cam(0, 0, 800, 600);

	maplvl map("Tileset.png", 48, 48);
	map.setMap(TileMap);
	map.setSizeMap(40, 25);
	

	map.addBlock(setBlock("0",192,0));
	map.addBlock(setBlock(" ", 0, 0));

	map.addBlock(setBlock("1", 0, 144));
	map.addBlock(setBlock("2", 576, 96));
	map.addBlock(setBlock("3", 624, 96));
	map.addBlock(setBlock("4", 672, 96));

	map.addBlock(setBlock("l", 288, 0));
	map.addBlock(setBlock("r", 384, 0));
	map.addBlock(setBlock("u", 432, 0));
	map.addBlock(setBlock("d", 336, 0));
	//map.addBlock(setBlock("�", 0, 0));

	hero.setMap(TileMap);

	Clock clock;
	float time;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
	
		time = clock.getElapsedTime().asMicroseconds(); //���� ��������� ����� � �������������
		clock.restart(); //������������� �����
		time = time / 800; //�������� ����
		hero.setTime(&time);

		hero.moveCharacter();
		cam.setCameraPosition(hero.getCoordX(), hero.getCoorgY());
		window.setView(cam.getView());
		window.clear();
		map.drawMap(window);
	
		window.draw(hero.getSprite());


		window.display();
	}

	return 0;
}