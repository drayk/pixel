#pragma once
#include <SFML/Graphics.hpp>
#include<iostream>
#include"enums.h"


class entity
{
protected:
	struct objectLvl
	{
		float _X, _Y;
		std::string _name;
		float _width, _height;
	};
	std::vector<objectLvl*> _object;


	sf::Image  _Image;
	sf::Texture _Texture;
	sf::Sprite _Sprite;

	float _posX = 0, _posY = 0; 
	float _width = 0, _height = 0;
	float _imageX = 0, _imageY = 0;
	float _dx = 0, _dy = 0;

	rotation _dir = RIGHT;

	float _speed = 0;

	int health = 100;

	bool life = true;
	float* _time;
	float _moveTimer;


public:
	entity(sf::String image, float ImageX, float ImageY, float width, float height, float posx, float posy)
	{
		setImage(image);
		setPosition(posx, posy);
		setRect(ImageX, ImageY, width, height);
		setSprite();

	}
	virtual void update() = 0;
	void setImage(sf::String images)
	{
		_Image.loadFromFile("image/"+ images);
	}
	void setPosition(int posx, int posy)
	{
		_posX = posx;
		_posY = posy;
	}
	void setRect(float posImageX, float posImageY, int  widthImage, int heightImage)
	{
		_width = widthImage; _height = heightImage;
		_imageX = posImageX; _imageY = posImageY;

	}
	void setSprite()
	{
		_Texture.loadFromImage(_Image);
		_Sprite.setTexture(_Texture);//������� � ���� ������ Texture (��������)
		_Sprite.setPosition(_posX, _posY);//������ ��������� ���������� ��������� �������
		_Sprite.setTextureRect(sf::IntRect(_imageX, _imageY, _width, _height));//�������� ������ ��� ������������� � �����
	}
	void setTime(float* time)
	{
		_time = time;
	}
	void setSpeed(float speed)
	{
		_speed = speed;
	}

	float getCoordX()const
	{

		std::cout << "X: " << _posX << "\t";
		return _posX;
	}
	float getCoorgY()const
	{
		std::cout << "Y: " << _posY << std::endl;
		return _posY;
	}
	
	sf::Sprite & getSprite()
	{
		return _Sprite;
	}
	sf::FloatRect getRect()
	{//�-��� ��������� ��������������. ��� �����,������� (���,�����).
		return sf::FloatRect(_posX, _posY, _width, _height);//��� �-��� ����� ��� �������� ������������ 
	}

};

