#pragma once
#include "character.h"
class player :
	public character
{
public:
	player()=default;
	player(sf::String image, float posImageCharacterX, float posImageCharacterY, float widthImageCharacter, float heightImageCharacter, float posx, float posy) 
		:character(image, posImageCharacterX, posImageCharacterY, widthImageCharacter, heightImageCharacter, posx, posy){}
	virtual void moveCharacter() override;

	~player();
};

